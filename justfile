GITLAB_PRIVATE_TOKEN := env_var("GITLAB_PRIVATE_TOKEN")
VERSION := env_var("IMAGE_VERSION")

build: 
    cargo build

test: 
    @GITLAB_PRIVATE_TOKEN={{GITLAB_PRIVATE_TOKEN}} cargo test

clear-pipelines:
    @GITLAB_PRIVATE_TOKEN={{GITLAB_PRIVATE_TOKEN}} cargo test --test resource_out_integration_tests -- out_remove_all_pipelines --ignored

test-trace:
    @GITLAB_PRIVATE_TOKEN={{GITLAB_PRIVATE_TOKEN}} RUST_BACKTRACE=1 cargo test

clear-pipelines-trace:
    @RUST_BACKTRACE=1 GITLAB_PRIVATE_TOKEN={{GITLAB_PRIVATE_TOKEN}} \
        cargo test --test resource_out_integration_tests -- out_remove_all_pipelines --ignored

push:
    docker push pms1969/gitlab-merge-request-resource:{{VERSION}}

build-and-push: docker-build push

docker-build:
    docker build -t pms1969/gitlab-merge-request-resource:{{VERSION}} .