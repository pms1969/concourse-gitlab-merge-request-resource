# Step 1: build the binary in release mode using musl
FROM clux/muslrust AS build

RUN mkdir -p /src
WORKDIR /src
COPY . /src

RUN cargo build --release
RUN strip target/x86_64-unknown-linux-musl/release/resource
RUN cp target/x86_64-unknown-linux-musl/release/resource .

# Step 2: retrieve SSL certificates
FROM alpine as certs

RUN apk update && apk add ca-certificates

# Step 3: create final image with the binary at the expected places
# and the SSL certificates
FROM busybox:musl

COPY --from=certs /etc/ssl/certs /etc/ssl/certs
ENV SSL_CERT_FILE /etc/ssl/certs/ca-certificates.crt
ENV SSL_CERT_DIR /etc/ssl/certs

COPY --from=build /src/resource /opt/resource/concourse-resource-gitlab

RUN ln -s /opt/resource/concourse-resource-gitlab /opt/resource/check  \
    & ln -s /opt/resource/concourse-resource-gitlab /opt/resource/out \
    & ln -s /opt/resource/concourse-resource-gitlab /opt/resource/in
