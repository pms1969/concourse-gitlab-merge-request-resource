use std::env;

use chrono::{DateTime, NaiveDateTime, Utc};
use concourse_resource::Resource;
use concourse_resource_gitlab::{GitlabMergeRequest, Source, Version};
use gitlab::{
    api::{projects::repository::commits::CommitBuilder, Query},
    Commit, Gitlab,
};

#[test]
fn test_check_works_when_no_mrs_present() {
    let key = "GITLAB_PRIVATE_TOKEN";
    let token = env::var(key).unwrap();

    let source = Source {
        uri: String::from("https://gitlab.com/concourse-int-tests-repos/test-repo-no-mrs"),
        username: Some(String::from("int-test-token")),
        private_token: token,
        ..Default::default()
    };
    let versions = GitlabMergeRequest::resource_check(Some(source), None);
    assert_eq!(versions.len(), 0);
}

#[test]
fn test_check_works_one_mr_before() {
    let key = "GITLAB_PRIVATE_TOKEN";
    let token = env::var(key).unwrap();

    let source = Source {
        uri: String::from("https://gitlab.com/concourse-int-tests-repos/test-repo-one-mr"),
        username: Some(String::from("int-test-token")),
        private_token: token,
        ..Default::default()
    };
    let ver = Version {
        sha: String::from("abcdef"),
        project_id: 32895433,
        mr_iid: 1,
        last_updated_at: DateTime::<Utc>::from_utc(NaiveDateTime::from_timestamp(1, 0), Utc),
        source_branch: String::from("fixbug"),
    };
    let versions = GitlabMergeRequest::resource_check(Some(source), Some(ver));
    assert_eq!(versions.len(), 2);
    assert_eq!(versions[1].sha, "f600b9bf585679593fbb42ccb90c4e6a8c219950");
}

#[test]
fn test_check_works_one_mr_after() {
    let key = "GITLAB_PRIVATE_TOKEN";
    let token = env::var(key).unwrap();

    let source = Source {
        uri: String::from("https://gitlab.com/concourse-int-tests-repos/test-repo-one-mr"),
        username: Some(String::from("int-test-token")),
        private_token: token,
        ..Default::default()
    };
    let date_str = "2022-01-17T14:22:28.000+00:00";
    // convert the string into DateTime<FixedOffset>
    let date_time = DateTime::parse_from_rfc3339(date_str).unwrap();
    // convert the string into DateTime<Utc> or other timezone
    let date_time_utc = date_time.with_timezone(&Utc);

    let ver = Version {
        sha: String::from("abcde"),
        project_id: 32895433,
        mr_iid: 1,
        last_updated_at: date_time_utc,
        source_branch: String::from("fixbug"),
    };
    let versions = GitlabMergeRequest::resource_check(Some(source), Some(ver));
    assert_eq!(versions.len(), 1);
}

#[test]
fn test_check_works_one_mr_plus_wip_before() {
    let key = "GITLAB_PRIVATE_TOKEN";
    let token = env::var(key).unwrap();

    let source = Source {
        uri: String::from("https://gitlab.com/concourse-int-tests-repos/test-repo-one-mr-plus-wip"),
        username: Some(String::from("int-test-token")),
        private_token: token,
        ..Default::default()
    };
    let ver = Version {
        sha: String::from("abcdef"),
        project_id: 32895433,
        mr_iid: 1,
        last_updated_at: DateTime::<Utc>::from_utc(NaiveDateTime::from_timestamp(1, 0), Utc),
        source_branch: String::from("fixbug"),
    };
    let versions = GitlabMergeRequest::resource_check(Some(source), Some(ver));
    assert_eq!(versions.len(), 2);
    assert_eq!(versions[0].sha, "abcdef");
    assert_eq!(versions[1].sha, "4c8ec852c29bde405808ecfbeeeaad179cf34809");
}

#[test]
fn test_check_works_filters_default_target_branch() {
    let key = "GITLAB_PRIVATE_TOKEN";
    let token = env::var(key).unwrap();

    let source = Source {
        uri: String::from("https://gitlab.com/concourse-int-tests-repos/test-repo-one-mr-plus-wip"),
        username: Some(String::from("int-test-token")),
        private_token: token,
        ..Default::default()
    };
    let ver = Version {
        sha: String::from("abcdef"),
        project_id: 32895433,
        mr_iid: 1,
        last_updated_at: DateTime::<Utc>::from_utc(NaiveDateTime::from_timestamp(1, 0), Utc),
        source_branch: String::from("fixbug"),
    };
    let versions = GitlabMergeRequest::resource_check(Some(source), Some(ver));
    assert_eq!(versions.len(), 2);
    assert_eq!(versions[0].sha, "abcdef");
    assert_eq!(versions[1].sha, "4c8ec852c29bde405808ecfbeeeaad179cf34809");
}

#[test]
fn test_check_works_filters_defined_target_branch() {
    let key = "GITLAB_PRIVATE_TOKEN";
    let token = env::var(key).unwrap();

    let source = Source {
        uri: String::from("https://gitlab.com/concourse-int-tests-repos/test-repo-one-mr-plus-wip"),
        username: Some(String::from("int-test-token")),
        private_token: token,
        merge_into: Some(String::from("development")),
        ..Default::default()
    };
    let ver = Version {
        sha: String::from("abcdef"),
        project_id: 32895433,
        mr_iid: 1,
        last_updated_at: DateTime::<Utc>::from_utc(NaiveDateTime::from_timestamp(1, 0), Utc),
        source_branch: String::from("fixbug"),
    };
    let versions = GitlabMergeRequest::resource_check(Some(source), Some(ver));
    assert_eq!(versions.len(), 1);
    assert_eq!(versions[0].sha, "abcdef");
}

#[test]
fn test_check_works_filters_defined_target_branch_existing() {
    let key = "GITLAB_PRIVATE_TOKEN";
    let token = env::var(key).unwrap();

    let source = Source {
        uri: String::from("https://gitlab.com/concourse-int-tests-repos/test-repo-one-mr-plus-wip"),
        username: Some(String::from("int-test-token")),
        private_token: token,
        merge_into: Some(String::from("ma.*")),
        ..Default::default()
    };
    let ver = Version {
        sha: String::from("abcdef"),
        project_id: 32895433,
        mr_iid: 1,
        last_updated_at: DateTime::<Utc>::from_utc(NaiveDateTime::from_timestamp(1, 0), Utc),
        source_branch: String::from("fixbug"),
    };
    let versions = GitlabMergeRequest::resource_check(Some(source), Some(ver));
    assert_eq!(versions.len(), 2);
    assert_eq!(versions[0].sha, "abcdef");
}

#[test]
fn check_gitlab_api_client() {
    let key = "GITLAB_PRIVATE_TOKEN";
    let token = env::var(key).unwrap();

    // this should just pass.  If it doesn't something is wrong with the token.
    let uri = String::from("gitlab.com");
    let _ = Gitlab::new(uri, token).unwrap();
}

#[test]
fn check_gitlab_get_commit_status() {
    let key = "GITLAB_PRIVATE_TOKEN";
    let token = env::var(key).unwrap();

    let uri = String::from("gitlab.com");
    let client = Gitlab::new(uri, token).unwrap();
    let endpoint = CommitBuilder::default()
        .project("concourse-int-tests-repos/test-repo-one-mr")
        .commit("f600b9bf585679593fbb42ccb90c4e6a8c219950")
        .build()
        .unwrap();
    let _: Commit = endpoint.query(&client).unwrap();
}
