use std::{env, fs};

use chrono::{DateTime, Utc};
use concourse_resource::Resource;
use concourse_resource_gitlab::{GitlabMergeRequest, InParams, Metadata, Source, Version};
use tempdir::TempDir;

#[test]
fn test_in_works_for_mr() {
    let key = "GITLAB_PRIVATE_TOKEN";
    let token = env::var(key).unwrap();

    let source = Source {
        uri: String::from("https://gitlab.com/concourse-int-tests-repos/test-repo-one-mr"),
        private_token: token,
        ..Default::default()
    };

    let date_str = "2022-01-17T13:22:28.000+00:00";
    // convert the string into DateTime<FixedOffset>
    let date_time = DateTime::parse_from_rfc3339(date_str).unwrap();
    // convert the string into DateTime<Utc> or other timezone
    let date_time_utc = date_time.with_timezone(&Utc);

    let ver = Version {
        sha: String::from("f600b9bf585679593fbb42ccb90c4e6a8c219950"),
        project_id: 32895433,
        mr_iid: 1,
        last_updated_at: date_time_utc,
        source_branch: String::from("fixbug"),
    };

    let _in_params = InParams {};
    let tmp_dir = TempDir::new("repos").unwrap();
    let path = tmp_dir.path().to_str().unwrap();

    let result = GitlabMergeRequest::resource_in(Some(source), ver, None, path).unwrap();
    // assert_eq!(versions.len(), 0);
    println!("{md}", md = result.metadata.unwrap().source_branch);
    let git_dir = tmp_dir.path().join(".git");
    let md_path = git_dir.join(".metadata");
    let filename = md_path.as_path().to_owned();
    let contents = fs::read_to_string(filename).expect("Something went wrong reading the file");
    println!("{contents}");
    let meta: Metadata =
        serde_json::from_str(contents.as_str()).expect("metadata cannot be deserialised");
    assert_eq!(
        meta.commit,
        String::from("f600b9bf585679593fbb42ccb90c4e6a8c219950")
    );
}

#[test]
fn test_in_works_for_mr_plus_draft() {
    let key = "GITLAB_PRIVATE_TOKEN";
    let token = env::var(key).unwrap();

    let source = Source {
        uri: String::from("https://gitlab.com/concourse-int-tests-repos/test-repo-one-mr-plus-wip"),
        private_token: token,
        ..Default::default()
    };

    let date_str = "2022-01-17T15:30:28.000+00:00";
    // convert the string into DateTime<FixedOffset>
    let date_time = DateTime::parse_from_rfc3339(date_str).unwrap();
    // convert the string into DateTime<Utc> or other timezone
    let date_time_utc = date_time.with_timezone(&Utc);

    let ver = Version {
        sha: String::from("4c8ec852c29bde405808ecfbeeeaad179cf34809"),
        project_id: 32900463,
        mr_iid: 1,
        last_updated_at: date_time_utc,
        source_branch: String::from("fixbug"),
    };

    let _in_params = InParams {};
    let tmp_dir = TempDir::new("repos").unwrap();
    let path = tmp_dir.path().to_str().unwrap();

    let result = GitlabMergeRequest::resource_in(Some(source), ver, None, path).unwrap();
    // assert_eq!(versions.len(), 0);
    println!("{md}", md = result.metadata.unwrap().source_branch);
}
