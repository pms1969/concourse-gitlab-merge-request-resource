use std::env;

use chrono::{DateTime, Utc};
use concourse_resource::Resource;
use concourse_resource_gitlab::utils::gitlab_adapter::PipelineStatus;
use concourse_resource_gitlab::{GitlabMergeRequest, OutParams, Source, Version};

use gitlab::api::projects::pipelines::{DeletePipelineBuilder, PipelinesBuilder};
use gitlab::api::projects::repository::commits::{CommitStatusState, CreateCommitStatusBuilder};
use gitlab::{api, CommitStatus, PipelineBasic};
use gitlab::{api::Query, Gitlab};
use tempdir::TempDir;

#[test]
fn test_out_works_for_mr() {
    env::set_var("BUILD_ID", "1");
    env::set_var("BUILD_TEAM_NAME", "QA");
    env::set_var("ATC_EXTERNAL_URL", "https://concourse.example.com");
    env::set_var("BUILD_PIPELINE_NAME", "out_test");
    env::set_var("BUILD_JOB_NAME", "test_job");
    env::set_var("BUILD_NAME", "1");

    let key = "GITLAB_PRIVATE_TOKEN";
    let token = env::var(key).unwrap();

    let in_source = Source {
        uri: String::from("https://gitlab.com/concourse-int-tests-repos/test-repo-one-mr"),
        private_token: token.clone(),
        ..Default::default()
    };
    let date_str = "2022-01-17T13:22:28.000+00:00";
    // convert the string into DateTime<FixedOffset>
    let date_time = DateTime::parse_from_rfc3339(date_str).unwrap();
    // convert the string into DateTime<Utc> or other timezone
    let date_time_utc = date_time.with_timezone(&Utc);

    let ver = Version {
        sha: String::from("f600b9bf585679593fbb42ccb90c4e6a8c219950"),
        project_id: 32895433,
        mr_iid: 1,
        last_updated_at: date_time_utc,
        source_branch: String::from("fixbug"),
    };

    let tmp_dir = TempDir::new("repos").unwrap();
    let path = tmp_dir.path().to_str().unwrap();

    let _ = GitlabMergeRequest::resource_in(Some(in_source), ver, None, path).unwrap();

    let out_source = Source {
        uri: String::from("https://gitlab.com/concourse-int-tests-repos/test-repo-one-mr"),
        private_token: token.clone(),
        ..Default::default()
    };

    let out_params = OutParams {
        repository: path.to_owned(),
        status: PipelineStatus::Running,
        build_label: None,
    };

    let _ = GitlabMergeRequest::resource_out(Some(out_source), Some(out_params), path);

    let finish_source = Source {
        uri: String::from("https://gitlab.com/concourse-int-tests-repos/test-repo-one-mr"),
        private_token: token,
        ..Default::default()
    };

    let finish_params = OutParams {
        repository: path.to_owned(),
        status: PipelineStatus::Success,
        build_label: None,
    };

    let _ = GitlabMergeRequest::resource_out(Some(finish_source), Some(finish_params), path);
}

#[test]
#[ignore]
fn out_update_status() {
    let key = "GITLAB_PRIVATE_TOKEN";
    let token = env::var(key).unwrap();

    let uri = String::from("gitlab.com");
    let client = Gitlab::new(uri, token).unwrap();

    let endpoint = CreateCommitStatusBuilder::default()
        .project(32895433)
        .commit(String::from("f600b9bf585679593fbb42ccb90c4e6a8c219950"))
        .state(CommitStatusState::Canceled)
        .name("mr-job")
        .target_url(String::from("https://example.com/ci/build"))
        .build()
        .unwrap();

    let _: CommitStatus = endpoint.query(&client).unwrap();
}

#[test]
#[ignore]
fn out_remove_all_pipelines() {
    let key = "GITLAB_PRIVATE_TOKEN";
    let token = env::var(key).unwrap();

    let uri = String::from("gitlab.com");
    let client = Gitlab::new(uri, token).unwrap();

    let endpoint = PipelinesBuilder::default()
        .project(32895433)
        .build()
        .unwrap();

    let pipelines: Vec<PipelineBasic> = api::paged(endpoint, api::Pagination::Limit(100))
        .query(&client)
        .unwrap();

    for p in pipelines.into_iter() {
        let ep = DeletePipelineBuilder::default()
            .project(32895433)
            .pipeline(p.id.value())
            .build()
            .unwrap();
        api::ignore(ep).query(&client).unwrap();
    }
}
