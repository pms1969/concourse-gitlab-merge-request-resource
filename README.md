# GitLab Merge Request Concourse Resource

A concourse resource to check for new merge requests on GitLab and update the merge request status.

## Source Configuration

```yaml
resource_types:
- name: merge-request
  type: docker-image
  source:
    repository: pms1969/gitlab-merge-request-resource

resources:
- name: repo-mr
  type: merge-request
  source:
    uri: https://gitlab.com/myname/myproject.git
    private_token: XXX
    username: my_username
    password: xxx
    merge_into: development
```

* `uri`: The location of the repository (required)
* `merge_into`: The target branch(s) to filter MR's for.  This is a regex, and defaults to `(master|main)`
* `private_token`: Your GitLab user's private token (required, can be found in your profile settings)
* `private_key`: Optional(not required for public repos) The private SSH key for SSH auth when pulling

  Example:

  ```yaml
  private_key: |
    -----BEGIN RSA PRIVATE KEY-----
    MIIEowIBAAKCAQEAtCS10/f7W7lkQaSgD/mVeaSOvSF9ql4hf/zfMwfVGgHWjj+W
    <Lots more text>
    DWiJL+OFeg9kawcUL6hQ8JeXPhlImG6RTUffma9+iGQyyBMCGd1l
    -----END RSA PRIVATE KEY-----
  ```

* `username`: Optional(not required for public repos) Username to use for basic authentication.  Prefer private_key over username/password.
* `password`: Optional(not required for public repos) Password to use for basic authentication.  Required if username present.

> Please note that you have to provide either `private_key` or `username` and `password` for authenticated (private) repositories.

## Behavior

### `check`: Check for new merge requests

Checks if there are new merge requests or merge requests with new commits.

### `in`: Clone merge request source branch

`git clone`s the source branch of the respective merge request.

### `out`: Update a merge request's merge status

Updates the merge request's `merge_status` which displays nicely in the GitLab UI and allows to only merge changes if they pass the test.

#### Parameters

* `repository`: The path of the repository of the merge request's source branch (required)
* `status`: The new status of the merge request (required, can be either `pending`, `running`, `success`, `failed`, or `canceled`)
* `build_label`: The label of the build in GitLab (optional, defaults to the name of the Concourse Job.)  The manifests as a Stage on the Gitlab Pipeline.

## Example

```yaml
jobs:
- name: test-merge-request
  plan:
  - get: repo
    resource: repo-mr
    trigger: true
  - put: repo-mr
    params:
      repository: repo
      status: running
      build_label: mr-job
  - task: run-tests
    file: repo/ci/tasks/run-tests.yml
  on_failure:
    put: repo-mr
    params:
      repository: repo
      status: failed
      build_label: mr-job
  on_success:
    put: repo-mr
    params:
      repository: repo
      status: success
      build_label: mr-job
```