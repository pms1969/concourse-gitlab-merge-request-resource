use std::sync::atomic::{AtomicI64, Ordering};

use crate::*;
use chrono::{DateTime, NaiveDateTime, Utc};
use concourse_resource::Resource;
use gitlab::Gitlab;

#[test]
fn check_one_current_with_no_previous() {
    //* Specifically test the case were we are running for the first time and do not
    //* have any previous version, and do have 1 new commit on an open, non WIP MR.

    fn callback_mrs(_: Option<Gitlab>, _: &str, _: &str) -> Vec<MR> {
        // This is our new MR with commit.
        vec![MR {
            id: 1,
            iid: 1,
            sha: String::from("abcdef"),
            source_branch: String::from("feature/test"),
            target_branch: String::from("master"),
            project_id: 6,
            work_in_progress: false,
            title: String::from("Test"),
            url: String::from(""),
            merge_status: MergeStatus::Other,
        }]
    }
    fn callback_last_update(_: Option<Gitlab>, _: &str, _: &str) -> DateTime<Utc> {
        // This is when it was last updated (epoch).
        DateTime::<Utc>::from_utc(NaiveDateTime::from_timestamp(61, 0), Utc)
    }
    let gitlab_adapter =
        GitlabAdapter::new(Some(callback_mrs), Some(callback_last_update), None, None);
    let git_adapter = GitAdapter::new(None);

    // Set up the source.
    let source = Source {
        uri: String::from("git@gitlab.com:test/test/test_repo.git"),
        private_token: String::from(""),
        gitlab_adapter: gitlab_adapter,
        git_adapter: git_adapter,
        ..Default::default()
    };

    let version = GitlabMergeRequest::resource_check(Some(source), None);

    // And in theory, we should get back 1 version, which points at the MR we found.
    assert_eq!(version.len(), 1);
    assert_eq!(version[0].sha, "abcdef");
    assert_eq!(version[0].mr_iid, 1);
    assert_eq!(version[0].project_id, 6);
}

#[test]
fn check_no_current_with_no_previous() {
    //* Specifically test the case were we are running for the first time and do not
    //* have any previous version, and there are no open, non WIP MR's.

    fn callback_mrs(_: Option<Gitlab>, _: &str, _: &str) -> Vec<MR> {
        // The empty set of MR's
        vec![]
    }
    let adapter = GitlabAdapter::new(Some(callback_mrs), None, None, None);
    let git_adapter = GitAdapter::new(None);

    // Set up the source
    let source = Source {
        uri: String::from("git@gitlab.com:test/test/test_repo.git"),
        private_token: String::from(""),
        gitlab_adapter: adapter,
        git_adapter: git_adapter,
        ..Default::default()
    };
    let version = GitlabMergeRequest::resource_check(Some(source), None);

    // And we should get nothing back.
    assert_eq!(version.len(), 0);
}

#[test]
fn check_one_current_with_previous() {
    //* Specifically test the case were we are running after already having identified MR's with changes.
    //* In this case, we have 1 new MR with a commit after the previous one.

    fn callback_mrs(_: Option<Gitlab>, _: &str, _: &str) -> Vec<MR> {
        // The latest MR
        vec![MR {
            id: 2,
            iid: 1,
            sha: String::from("sha2"),
            source_branch: String::from("feature/branch2"),
            target_branch: String::from("master"),
            project_id: 1,
            work_in_progress: false,
            title: String::from("Test"),
            url: String::from(""),
            merge_status: MergeStatus::Other,
        }]
    }
    fn callback_last_update(_: Option<Gitlab>, _: &str, _: &str) -> DateTime<Utc> {
        // Commit created at after previous versions last_updated_at
        DateTime::<Utc>::from_utc(NaiveDateTime::from_timestamp(61, 0), Utc)
    }
    let adapter = GitlabAdapter::new(Some(callback_mrs), Some(callback_last_update), None, None);
    let git_adapter = GitAdapter::new(None);

    // Set up the source.
    let source = Source {
        uri: String::from("git@gitlab.com:test/test/test_repo.git"),
        private_token: String::from(""),
        gitlab_adapter: adapter,
        git_adapter: git_adapter,
        ..Default::default()
    };

    // Previous version with earlier commit.
    let previous = Version {
        sha: String::from("sha1"),
        last_updated_at: DateTime::<Utc>::from_utc(NaiveDateTime::from_timestamp(1, 0), Utc),
        project_id: 42,
        mr_iid: 7,
        source_branch: String::from("fixbug"),
    };
    let version = GitlabMergeRequest::resource_check(Some(source), Some(previous));

    // The previous version should appear first, followed by the current version.
    //* as outlined here: https://concourse-ci.org/implementing-resource-types.html#resource-check
    assert_eq!(version.len(), 2);
    assert_eq!(version[0].sha, "sha1");
    assert_eq!(version[1].sha, "sha2");
}

#[test]
fn check_multiple_current_with_previous() {
    //* Specifically test the case were we are running after already having identified MR's with changes.
    //* In this case, we have 2 new MR with a commit after the previous one.

    fn callback_mrs(_: Option<Gitlab>, _: &str, _: &str) -> Vec<MR> {
        // Our 2 new MRs
        vec![
            MR {
                id: 2,
                iid: 1,
                sha: String::from("sha2"),
                source_branch: String::from("feature/branch2"),
                target_branch: String::from("master"),
                project_id: 7,
                work_in_progress: false,
                title: String::from("Test"),
                url: String::from(""),
                merge_status: MergeStatus::Other,
            },
            MR {
                id: 4,
                iid: 1,
                sha: String::from("sha4"),
                source_branch: String::from("feature/branch4"),
                target_branch: String::from("master"),
                project_id: 8,
                work_in_progress: false,
                title: String::from("Test"),
                url: String::from(""),
                merge_status: MergeStatus::Other,
            },
        ]
    }
    // We want the commit created_at date to be deterministic.
    // Incrementing the atomic means that each subsequent call has a higher number
    // Therefore, MR.id 4 has a later commit date that MR.id 2.
    static CALL_COUNT: AtomicI64 = AtomicI64::new(1);
    fn callback_last_update(_: Option<Gitlab>, _: &str, _: &str) -> DateTime<Utc> {
        CALL_COUNT.fetch_add(1, Ordering::SeqCst);
        let secs = CALL_COUNT.load(Ordering::Relaxed);
        DateTime::<Utc>::from_utc(NaiveDateTime::from_timestamp(secs, 0), Utc)
    }
    let adapter = GitlabAdapter::new(Some(callback_mrs), Some(callback_last_update), None, None);
    let git_adapter = GitAdapter::new(None);

    // Set up the source
    let source = Source {
        uri: String::from("git@gitlab.com:test/test/test_repo.git"),
        private_token: String::from(""),
        gitlab_adapter: adapter,
        git_adapter: git_adapter,
        ..Default::default()
    };

    let previous = Version {
        sha: String::from("sha1"),
        last_updated_at: DateTime::<Utc>::from_utc(NaiveDateTime::from_timestamp(1, 0), Utc),
        project_id: 42,
        mr_iid: 8,
        source_branch: String::from("fixbug"),
    };
    let version = GitlabMergeRequest::resource_check(Some(source), Some(previous));

    // The previous version should appear first, followed by the current versions in ascending commit date order.
    //* as outlined here: https://concourse-ci.org/implementing-resource-types.html#resource-check
    assert_eq!(version.len(), 3);
    assert_eq!(version[0].sha, "sha1");
    assert_eq!(version[1].sha, "sha2");
    assert_eq!(version[2].sha, "sha4");
}

#[test]
fn check_multiple_current_with_previous_not_all_commits() {
    //* Specifically test the case were we are running after already having identified MR's with changes.
    //* In this case, we have 2 new MRs. One does not have a commit after the previous one.

    fn callback_mrs(_: Option<Gitlab>, _: &str, _: &str) -> Vec<MR> {
        // MR.id 2 will return a last_updated_at of 3, which is less than the 5 of the
        // previous version.  It should therefore not be added to the version output.
        vec![
            MR {
                id: 2,
                iid: 1,
                sha: String::from("sha2"),
                source_branch: String::from("feature/branch2"),
                target_branch: String::from("master"),
                project_id: 1,
                work_in_progress: false,
                title: String::from("Test"),
                url: String::from(""),
                merge_status: MergeStatus::Other,
            },
            MR {
                id: 4,
                iid: 1,
                sha: String::from("sha4"),
                source_branch: String::from("feature/branch4"),
                target_branch: String::from("master"),
                project_id: 1,
                work_in_progress: false,
                title: String::from("Test"),
                url: String::from(""),
                merge_status: MergeStatus::Other,
            },
        ]
    }
    // We want the commit created_at date to be deterministic.
    // Incrementing the atomic means that each subsequent call has a higher number
    // Therefore, MR.id 4 has a later commit date that MR.id 2.
    static CALL_COUNT: AtomicI64 = AtomicI64::new(0);
    fn callback_last_update(_: Option<Gitlab>, _: &str, _: &str) -> DateTime<Utc> {
        CALL_COUNT.fetch_add(3, Ordering::SeqCst);
        let secs = CALL_COUNT.load(Ordering::Relaxed);
        DateTime::<Utc>::from_utc(NaiveDateTime::from_timestamp(secs, 0), Utc)
    }
    let adapter = GitlabAdapter::new(Some(callback_mrs), Some(callback_last_update), None, None);
    let git_adapter = GitAdapter::new(None);

    // Set up the source.
    let source = Source {
        uri: String::from("git@gitlab.com:test/test/test_repo.git"),
        private_token: String::from(""),
        gitlab_adapter: adapter,
        git_adapter: git_adapter,
        ..Default::default()
    };

    // The previous version has a last_updated_at after MR.id 2, but before MR.id 4
    let previous = Version {
        sha: String::from("sha1"),
        last_updated_at: DateTime::<Utc>::from_utc(NaiveDateTime::from_timestamp(5, 0), Utc),
        project_id: 42,
        mr_iid: 3,
        source_branch: String::from("fixbug"),
    };
    let version = GitlabMergeRequest::resource_check(Some(source), Some(previous));

    // The previous version should appear first, followed by the current versions in ascending commit date order.
    //* as outlined here: https://concourse-ci.org/implementing-resource-types.html#resource-check
    // We should have the following order of commits.  MR.id 1, skip MR.id 2, MR.id 4
    assert_eq!(version.len(), 2);
    assert_eq!(version[0].sha, "sha1");
    assert_eq!(version[1].sha, "sha4");
}

#[test]
fn check_out_of_order_mrs_are_ordered_correctly() {
    //* Specifically test the case were we are running after already having identified MR's with changes.
    //* And receive the MR's out of order. This happens when an MR has been updated with a comment, but
    //* but not a commit.

    fn callback_mrs(_: Option<Gitlab>, _: &str, _: &str) -> Vec<MR> {
        vec![
            MR {
                id: 2,
                iid: 1,
                sha: String::from("sha2"),
                source_branch: String::from("feature/branch2"),
                target_branch: String::from("master"),
                project_id: 1,
                work_in_progress: false,
                title: String::from("Test"),
                url: String::from(""),
                merge_status: MergeStatus::Other,
            },
            MR {
                id: 4,
                iid: 1,
                sha: String::from("sha4"),
                source_branch: String::from("feature/branch4"),
                target_branch: String::from("master"),
                project_id: 1,
                work_in_progress: false,
                title: String::from("Test"),
                url: String::from(""),
                merge_status: MergeStatus::Other,
            },
            MR {
                id: 5,
                iid: 1,
                sha: String::from("sha5"),
                source_branch: String::from("feature/branch5"),
                target_branch: String::from("master"),
                project_id: 1,
                work_in_progress: false,
                title: String::from("Test"),
                url: String::from(""),
                merge_status: MergeStatus::Other,
            },
        ]
    }
    // Add 5 if the atomic is odd and subtract 3 if it is even.  This ensures that the second MR (MR.id 4)
    // has a lower last_update_at value than MR.id 2, and MR.id5
    static CALL_COUNT: AtomicI64 = AtomicI64::new(1);
    fn callback_last_update(_: Option<Gitlab>, _: &str, _: &str) -> DateTime<Utc> {
        if CALL_COUNT.load(Ordering::Relaxed) % 2 == 0 {
            CALL_COUNT.fetch_sub(3, Ordering::SeqCst);
        } else {
            CALL_COUNT.fetch_add(5, Ordering::SeqCst);
        }
        let secs = CALL_COUNT.load(Ordering::Relaxed);
        DateTime::<Utc>::from_utc(NaiveDateTime::from_timestamp(secs, 0), Utc)
    }
    let adapter = GitlabAdapter::new(Some(callback_mrs), Some(callback_last_update), None, None);
    let git_adapter = GitAdapter::new(None);

    let source = Source {
        uri: String::from("git@gitlab.com:test/test/test_repo.git"),
        private_token: String::from(""),
        gitlab_adapter: adapter,
        git_adapter: git_adapter,
        ..Default::default()
    };

    let previous = Version {
        sha: String::from("sha1"),
        last_updated_at: DateTime::<Utc>::from_utc(NaiveDateTime::from_timestamp(5, 0), Utc),
        project_id: 42,
        mr_iid: 6,
        source_branch: String::from("fixbug"),
    };
    let version = GitlabMergeRequest::resource_check(Some(source), Some(previous));

    // The previous version should appear first, followed by the current versions in ascending commit date order.
    //* as outlined here: https://concourse-ci.org/implementing-resource-types.html#resource-check
    // We should have the following order of commits.  MR.id 1, MR.id 2, skip MR.id 4, MR.id 5
    assert_eq!(version.len(), 3);
    assert_eq!(version[0].sha, "sha1");
    assert_eq!(version[1].sha, "sha2");
    assert_eq!(version[2].sha, "sha5");
}

#[test]
fn test_serialisation() {
    let s = r#"{ "sha": "123", "project_id": "1", "mr_iid": "2", "last_updated_at": "1970-01-01T00:00:05Z", "branch": "tst" }"#;
    let v: Version = serde_json::from_str(s).unwrap();
    assert_eq!(v.project_id, 1);
    assert_eq!(v.mr_iid, 2);

    let serialised = serde_json::to_string(&v).unwrap();
    assert_eq!(
        serialised,
        r#"{"branch":"tst","sha":"123","project_id":"1","mr_iid":"2","last_updated_at":"1970-01-01T00:00:05Z"}"#
    );
}
