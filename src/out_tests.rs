#[cfg(test)]
use std::fs::create_dir;
use std::io::Write;
use std::{env, fs::File};

use crate::*;
use chrono::{DateTime, NaiveDateTime, Utc};
use concourse_resource::Resource;
use gitlab::Gitlab;
use tempdir::TempDir;

#[test]
fn out_one_current_with_no_previous() {
    //* Specifically test the case were we are running for the first time and do not
    //* have any previous version, and do have 1 new commit on an open, non WIP MR.

    env::set_var("BUILD_ID", "1");
    env::set_var("BUILD_TEAM_NAME", "QA");
    env::set_var("ATC_EXTERNAL_URL", "https://concourse.example.com");
    env::set_var("BUILD_PIPELINE_NAME", "out_test");
    env::set_var("BUILD_JOB_NAME", "test_job");
    env::set_var("BUILD_NAME", "1");
    let tmp_dir = TempDir::new("example").unwrap();
    let git_path = tmp_dir.path().join(".git");
    create_dir(git_path.clone()).unwrap();
    let file_path = git_path.join(".metadata");
    println!("{}", file_path.to_str().unwrap());
    let mut tmp_file = File::create(file_path).unwrap();

    let md = Metadata {
        commit: String::from("abcdef"),
        author: String::from("the_author@example.com"),
        author_date: DateTime::<Utc>::from_utc(NaiveDateTime::from_timestamp(1, 0), Utc),
        committer: String::from("the_committer@example.com"),
        commit_date: DateTime::<Utc>::from_utc(NaiveDateTime::from_timestamp(1, 0), Utc),
        source_branch: String::from("sourceb"),
        target_branch: String::from("main"),
        title: String::from("The MR title."),
        message: String::from("A message for the MR."),
        url: String::from(""),
        mr_iid: 1,
        project_id: 2,
    };
    let metadata = serde_json::to_string(&md).unwrap();
    writeln!(tmp_file, "{metadata}").unwrap();

    let out_params = OutParams {
        repository: String::from(""),
        status: PipelineStatus::Running,
        build_label: Some(String::from("")),
    };
    fn pipeline_status(_: Option<Gitlab>, _: &str, _: &str, _: PipelineStatus, _: &str, _: &str) {}
    let gitlab_adapter = GitlabAdapter::new(None, None, Some(pipeline_status), None);
    let git_adapter = GitAdapter::new(None);

    // Set up the source.
    let source = Source {
        uri: String::from("git@gitlab.com:test/test/test_repo.git"),
        private_token: String::from(""),
        gitlab_adapter: gitlab_adapter,
        git_adapter: git_adapter,
        ..Default::default()
    };

    let input_path = tmp_dir.path().to_str().unwrap();
    let output = GitlabMergeRequest::resource_out(Some(source), Some(out_params), input_path);
    let version = output.version;

    drop(tmp_file);
    tmp_dir.close().unwrap();

    // And in theory, we should get back 1 version, which points at the MR we found.
    assert_eq!(version.sha, "abcdef");
}
