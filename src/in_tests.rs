use std::env;
use std::error::Error;

use crate::utils::git_adapter;
use crate::utils::gitlab_adapter::MergeStatus;
use crate::utils::gitlab_adapter::MR;
use crate::GitAdapter;
use crate::GitlabAdapter;
use crate::GitlabMergeRequest;
use crate::Source;
use crate::Version;
use chrono::{DateTime, NaiveDateTime, Utc};
use concourse_resource::Resource;
use gitlab::Gitlab;

#[test]
fn in_one_current_with_no_previous() {
    //* Specifically test the case were we are running for the first time and do not
    //* have any previous version, and do have 1 new commit on an open, non WIP MR.

    env::set_var("BUILD_ID", "1");
    env::set_var("BUILD_TEAM_NAME", "QA");
    env::set_var("ATC_EXTERNAL_URL", "https://concourse.example.com");
    env::set_var("BUILD_PIPELINE_NAME", "out_test");
    env::set_var("BUILD_JOB_NAME", "test_job");
    env::set_var("BUILD_NAME", "1");

    fn callback_clone_repo(
        _: &str,
        _: Option<&str>,
        _: Option<&str>,
        _: Option<&str>,
        _: &str,
        branch: &str,
    ) -> Result<git_adapter::Metadata, Box<dyn Error>> {
        let md = git_adapter::Metadata {
            sha: String::from("abcdef"),
            author: String::from("Arthur"),
            author_date: DateTime::<Utc>::from_utc(NaiveDateTime::from_timestamp(1, 0), Utc),
            committer: String::from("Ford"),
            commit_date: DateTime::<Utc>::from_utc(NaiveDateTime::from_timestamp(2, 0), Utc),
            branch: branch.into(),
            message: String::from("blah"),
        };
        Ok(md)
    }
    fn callback_get_mr_details(_: Option<Gitlab>, _: u64, _: u64) -> MR {
        MR {
            id: 1,
            iid: 2,
            sha: String::from("abcdef"),
            source_branch: String::from("source_branch"),
            target_branch: String::from("main"),
            project_id: 12,
            work_in_progress: false,
            title: String::from("This is the title"),
            url: String::from("https://gitlab.com/test/test"),
            merge_status: MergeStatus::Other,
        }
    }
    let gitlab_adapter = GitlabAdapter::new(None, None, None, Some(callback_get_mr_details));
    let git_adapter = GitAdapter::new(Some(callback_clone_repo));

    // Set up the source.
    let source = Source {
        uri: String::from("git@gitlab.com:test/test/test_repo.git"),
        private_token: String::from(""),
        gitlab_adapter: gitlab_adapter,
        git_adapter: git_adapter,
        ..Default::default()
    };

    let vin = Version {
        sha: String::from("abcdef"),
        project_id: 6,
        mr_iid: 7,
        last_updated_at: DateTime::<Utc>::from_utc(NaiveDateTime::from_timestamp(3, 0), Utc),
        source_branch: String::from("fixbug"),
    };
    let output = GitlabMergeRequest::resource_in(Some(source), vin, None, "")
        .expect("Unable to get resource.");
    let version = output.version;

    // And in theory, we should get back 1 version, which points at the MR we found.
    assert_eq!(version.sha, "abcdef");
}

#[test]
fn in_one_current_already_merged() {
    //* Specifically test the case were we are doing a `get` after a `put` and the merge request has delete source on merge set
    //* and merge_on_success set.  This causes the concourse job to fail, since the cloning cannot happen.
    env::set_var("BUILD_ID", "1");
    env::set_var("BUILD_TEAM_NAME", "QA");
    env::set_var("ATC_EXTERNAL_URL", "https://concourse.example.com");
    env::set_var("BUILD_PIPELINE_NAME", "out_test");
    env::set_var("BUILD_JOB_NAME", "test_job");
    env::set_var("BUILD_NAME", "1");

    fn callback_get_mr_details(_: Option<Gitlab>, _: u64, _: u64) -> MR {
        MR {
            id: 1,
            iid: 2,
            sha: String::from("abcdef"),
            source_branch: String::from("source_branch"),
            target_branch: String::from("main"),
            project_id: 12,
            work_in_progress: false,
            title: String::from("This is the title"),
            url: String::from("https://gitlab.com/test/test"),
            merge_status: MergeStatus::Merged,
        }
    }
    let gitlab_adapter = GitlabAdapter::new(None, None, None, Some(callback_get_mr_details));

    // Set up the source.
    let source = Source {
        uri: String::from("git@gitlab.com:test/test/test_repo.git"),
        private_token: String::from(""),
        gitlab_adapter: gitlab_adapter,
        ..Default::default()
    };

    let vin = Version {
        sha: String::from("abcdef"),
        project_id: 6,
        mr_iid: 7,
        last_updated_at: DateTime::<Utc>::from_utc(NaiveDateTime::from_timestamp(3, 0), Utc),
        source_branch: String::from("fixbug"),
    };
    let output = GitlabMergeRequest::resource_in(Some(source), vin, None, "")
        .expect("Unable to get resource.");
    let version = output.version;

    // And in theory, we should get back 1 version, which points at the MR we found.
    assert_eq!(version.sha, "abcdef");
    assert!(output.metadata.is_none());
}
