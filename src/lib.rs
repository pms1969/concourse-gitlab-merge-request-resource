#![allow(clippy::redundant_field_names)]

use chrono::{DateTime, NaiveDateTime, Utc};
use concourse_resource::*;
use serde::{Deserialize, Serialize};
use serde_with::serde_as;
use serde_with::DisplayFromStr;
use std::error::Error;
use std::fmt::Debug;
use std::path::Path;
use std::{fmt, fs};
use urlencoding::{self, encode};
use utils::git_adapter::GitAdapter;
use utils::gitlab_adapter::{GitlabAdapter, PipelineStatus, MR};

use crate::utils::gitlab_adapter::MergeStatus;
pub mod utils;

pub struct GitlabMergeRequest;
#[serde_as]
#[derive(Deserialize, Debug, Serialize, Clone)]
pub struct Version {
    #[serde(rename = "branch")]
    pub source_branch: String,
    pub sha: String,
    #[serde_as(as = "DisplayFromStr")]
    pub project_id: u64,
    #[serde_as(as = "DisplayFromStr")]
    pub mr_iid: u64,
    pub last_updated_at: DateTime<Utc>,
}

#[derive(Deserialize)]
pub struct Source {
    pub uri: String,
    #[serde(default = "default_merge_into")]
    pub merge_into: Option<String>,
    pub private_token: String,
    pub private_key: Option<String>,
    pub username: Option<String>,
    pub password: Option<String>,
    #[serde(skip)]
    pub gitlab_adapter: GitlabAdapter,
    #[serde(skip)]
    pub git_adapter: GitAdapter,
}

fn default_merge_into() -> Option<String> {
    Some(String::from("(master|main)"))
}

impl Default for Source {
    fn default() -> Self {
        Source {
            uri: String::from(""),
            merge_into: Some(String::from("(master|main)")),
            private_token: String::from(""),
            private_key: None,
            username: None,
            password: None,
            gitlab_adapter: GitlabAdapter::default(),
            git_adapter: GitAdapter::default(),
        }
    }
}
impl Source {
    fn set_client(&mut self, uri: &str, token: &str) {
        self.gitlab_adapter.set_client(uri, token);
    }

    fn get_latest_mrs(&self, path: &str, target_branch_regex: &str) -> Vec<MR> {
        self.gitlab_adapter
            .get_latest_mrs(path, target_branch_regex)
    }

    fn get_last_updated_at_for_mr(&self, path: &str, sha: &str) -> DateTime<Utc> {
        self.gitlab_adapter.get_last_updated_at_for_mr(path, sha)
    }

    fn get_mr_details(&self, project_id: u64, mr_iid: u64) -> MR {
        self.gitlab_adapter.get_mr_details(project_id, mr_iid)
    }

    fn clone_repo(
        &self,
        uri: &str,
        username: Option<&str>,
        password: Option<&str>,
        private_key: Option<&str>,
        path: &str,
        branch: &str,
    ) -> Result<utils::git_adapter::Metadata, Box<dyn Error>> {
        self.git_adapter
            .clone_repo(uri, username, password, private_key, path, branch)
    }
}

#[derive(Deserialize, Default)]
#[serde(default)]
pub struct InParams {}

#[derive(Serialize, Deserialize, Debug, IntoMetadataKV, Clone)]
pub struct Metadata {
    pub commit: String,
    pub author: String,
    pub author_date: DateTime<Utc>,
    pub committer: String,
    pub commit_date: DateTime<Utc>,
    pub source_branch: String,
    pub target_branch: String,
    pub title: String,
    pub message: String,
    pub url: String,
    pub mr_iid: u64,
    pub project_id: u64,
}

#[derive(Debug)]
struct InconsistentSHAError {
    details: String,
}

impl InconsistentSHAError {
    fn new(msg: &str) -> InconsistentSHAError {
        InconsistentSHAError {
            details: msg.to_string(),
        }
    }
}

impl fmt::Display for InconsistentSHAError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.details)
    }
}

impl std::error::Error for InconsistentSHAError {
    fn description(&self) -> &str {
        &self.details
    }
}

#[derive(Deserialize, Debug)]
#[serde(default)]
pub struct OutParams {
    pub repository: String,
    pub status: PipelineStatus,
    pub build_label: Option<String>,
}
impl Default for OutParams {
    fn default() -> Self {
        OutParams {
            repository: String::from(""),
            status: PipelineStatus::default(),
            build_label: None,
        }
    }
}

impl Resource for GitlabMergeRequest {
    type Version = Version;

    type Source = Source;

    type InParams = InParams;
    type InMetadata = Metadata;

    type OutParams = OutParams;
    type OutMetadata = Metadata;

    fn resource_check(
        source: Option<Self::Source>,
        version: Option<Self::Version>,
    ) -> Vec<Self::Version> {
        let (last, last_updated_at) = match version {
            Some(v) => {
                let last_updated_at = v.last_updated_at;
                (vec![v], last_updated_at)
            }
            None => (
                vec![],
                DateTime::<Utc>::from_utc(NaiveDateTime::from_timestamp(0, 0), Utc),
            ),
        };
        eprintln!("Last Version: {}", &last.len());
        let mut s = source.unwrap();
        eprintln!("URI: {}", &s.uri);

        let (_, domain, project_path) = utils::uri::deconstruct_uri(s.uri.as_str());
        let token = s.private_token.clone();
        s.set_client(&domain, &token);

        let target_branch_regex = s.merge_into.clone().unwrap();
        eprintln!("Target Branch: {}", &target_branch_regex);
        let mrs = s.get_latest_mrs(&project_path, &target_branch_regex);
        let mut all_mrs = mrs
            .into_iter()
            .filter(|mr| !mr.sha.is_empty())
            .filter_map(|mr| {
                let mr_last_updated = s.get_last_updated_at_for_mr(&project_path, &mr.sha);
                if mr_last_updated > last_updated_at {
                    Some(Version {
                        source_branch: mr.source_branch,
                        sha: mr.sha,
                        project_id: mr.project_id,
                        mr_iid: mr.iid,
                        last_updated_at: mr_last_updated,
                    })
                } else {
                    None
                }
            })
            .chain(last)
            .collect::<Vec<Version>>();
        all_mrs.sort_by(|a, b| a.last_updated_at.cmp(&b.last_updated_at));
        eprintln!("All MR's: {:?}", &all_mrs);
        all_mrs
    }

    fn resource_in(
        source: Option<Self::Source>,
        version: Self::Version,
        _params: Option<Self::InParams>,
        output_path: &str,
    ) -> Result<InOutput<Self::Version, Self::InMetadata>, Box<dyn std::error::Error>> {
        let mut s = source.unwrap();
        let (_, domain, _project_path) = utils::uri::deconstruct_uri(s.uri.as_str());
        let token = s.private_token.clone();
        s.set_client(&domain, &token);
        eprintln!("Source URI: {}", &s.uri);
        eprintln!("Version: {:?}", &version);

        let mr_details = s.get_mr_details(version.project_id, version.mr_iid);
        // If the pipeline is already merged, we don't want to try cloning the branch, since it may have been removed on merge
        // Resources doing the get after a put, has failed many a job for no practical reason.
        if mr_details.merge_status == MergeStatus::Merged {
            eprintln!("MR [{}] has already been merged.  Skipping clone of repo as the source branch may have been removed.", mr_details.iid);
            return Ok(InOutput {
                version: version,
                metadata: None,
            });
        }

        // Clone the repo locally
        let meta = s.clone_repo(
            &s.uri,
            s.username.as_deref(),
            s.password.as_deref(),
            s.private_key.as_deref(),
            output_path,
            &mr_details.source_branch,
        )?;

        if version.sha == meta.sha {
            let in_metadata = Self::InMetadata {
                commit: meta.sha,
                author: meta.author,
                author_date: meta.author_date,
                committer: meta.committer,
                commit_date: meta.commit_date,
                source_branch: meta.branch,
                target_branch: mr_details.target_branch,
                title: mr_details.title,
                message: meta.message,
                url: mr_details.url,
                mr_iid: mr_details.iid,
                project_id: mr_details.project_id,
            };

            // write the metadata to disk
            fs::write(
                Path::new(output_path).join(".git/.metadata"),
                serde_json::to_string(&in_metadata)?,
            )?;

            Ok(InOutput {
                version: version,
                metadata: Some(in_metadata),
            })
        } else {
            Err(Box::new(InconsistentSHAError::new(&format!(
                "HEAD of branch {} does not match version sha {}.",
                meta.sha, version.sha
            ))))
        }
    }

    fn resource_out(
        source: Option<Self::Source>,
        params: Option<Self::OutParams>,
        input_path: &str,
    ) -> OutOutput<Self::Version, Self::OutMetadata> {
        let bd = Self::build_metadata();
        let mut s = source.unwrap();
        let (_, domain, project) = utils::uri::deconstruct_uri(s.uri.as_str());
        let token = s.private_token.clone();
        s.set_client(&domain, &token);

        // unencoded versions of the concourse params.
        let upn = bd.pipeline_name.expect("missing pipeline name.");
        let ujn = bd.job_name.expect("missing job name");
        let ubn = bd.name.expect("missing build name");

        let team_name = encode(&bd.team_name);
        let pipeline_name = encode(&upn);
        let job_name = encode(&ujn);
        let build_name = encode(&ubn);
        let build_url = format!(
            "{}/teams/{team_name}/pipelines/{pipeline_name}/jobs/{job_name}/builds/{build_name}",
            bd.atc_external_url
        );

        let p = params.unwrap();
        eprintln!("Input Path: {}", &input_path);
        eprintln!("Repo: {}", &p.repository);
        eprintln!("Status: {:?}", &p.status);

        let filepath = Path::new(input_path)
            .join(&p.repository)
            .join(".git/.metadata");
        let meta: Self::OutMetadata =
            serde_json::from_str(&fs::read_to_string(filepath).expect("metadata file unreadable."))
                .expect("metadata cannot be deserialised");

        let label = match p.build_label {
            Some(l) => l,
            None => ujn,
        };

        s.gitlab_adapter.update_pipeline_status(
            &project,
            &meta.commit,
            p.status,
            &build_url,
            &label,
        );

        let output = OutOutput {
            version: Self::Version {
                source_branch: meta.source_branch.clone(),
                sha: meta.commit.clone(),
                project_id: meta.project_id,
                mr_iid: meta.mr_iid,
                last_updated_at: meta.commit_date,
            },
            metadata: Some(meta.clone()),
        };
        eprintln!("Version: {:?}", &output.version);
        eprintln!("Metadata: {:?}", &meta);
        output
    }

    fn build_metadata() -> BuildMetadata {
        BuildMetadata {
            id: std::env::var("BUILD_ID").expect("environment variable BUILD_ID should be present"),
            name: std::env::var("BUILD_NAME").ok(),
            job_name: std::env::var("BUILD_JOB_NAME").ok(),
            pipeline_name: std::env::var("BUILD_PIPELINE_NAME").ok(),
            pipeline_instance_vars: std::env::var("BUILD_PIPELINE_INSTANCE_VARS")
                .ok()
                .and_then(|instance_vars| serde_json::from_str(&instance_vars[..]).ok()),
            team_name: std::env::var("BUILD_TEAM_NAME")
                .expect("environment variable BUILD_TEAM_NAME should be present"),
            atc_external_url: std::env::var("ATC_EXTERNAL_URL")
                .expect("environment variable ATC_EXTERNAL_URL should be present"),
        }
    }
}

#[cfg(test)]
mod check_tests;

#[cfg(test)]
mod in_tests;

#[cfg(test)]
mod out_tests;
