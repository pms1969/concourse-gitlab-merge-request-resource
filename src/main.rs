#![deny(clippy::all)]
#![deny(clippy::nursery)]

use concourse_resource::*;
use concourse_resource_gitlab::GitlabMergeRequest;

create_resource!(GitlabMergeRequest);
