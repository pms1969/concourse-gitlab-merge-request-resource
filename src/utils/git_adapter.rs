#![allow(clippy::type_complexity)]

use std::{
    error::Error,
    fs::{self},
    path::Path,
};

use chrono::{DateTime, NaiveDateTime, Utc};
use git2::{Cred, RemoteCallbacks};
use serde::{Deserialize, Serialize};

pub struct GitAdapter {
    clone_repo_func: fn(
        uri: &str,
        username: Option<&str>,
        password: Option<&str>,
        private_key: Option<&str>,
        path: &str,
        branch: &str,
    ) -> Result<Metadata, Box<dyn Error>>,
}
impl Default for GitAdapter {
    fn default() -> Self {
        GitAdapter {
            clone_repo_func: default_clone_repo,
        }
    }
}

#[derive(Serialize, Deserialize, Clone)]
pub struct Metadata {
    pub sha: String,
    pub author: String,
    pub author_date: DateTime<Utc>,
    pub committer: String,
    pub commit_date: DateTime<Utc>,
    pub branch: String,
    pub message: String,
}

impl GitAdapter {
    #[cfg(test)]
    pub fn new(
        clone_repo_func: Option<
            fn(
                uri: &str,
                username: Option<&str>,
                password: Option<&str>,
                private_key: Option<&str>,
                path: &str,
                branch: &str,
            ) -> Result<Metadata, Box<dyn Error>>,
        >,
    ) -> Self {
        GitAdapter {
            clone_repo_func: match clone_repo_func {
                Some(f) => f,
                None => default_clone_repo,
            },
        }
    }

    pub fn clone_repo(
        &self,
        uri: &str,
        username: Option<&str>,
        password: Option<&str>,
        private_key: Option<&str>,
        path: &str,
        branch: &str,
    ) -> Result<Metadata, Box<dyn Error>> {
        (self.clone_repo_func)(uri, username, password, private_key, path, branch)
    }
}

fn default_clone_repo(
    uri: &str,
    username: Option<&str>,
    password: Option<&str>,
    private_key: Option<&str>,
    path: &str,
    branch: &str,
) -> Result<Metadata, Box<dyn Error>> {
    // Prepare callbacks.
    let mut callbacks = RemoteCallbacks::new();
    callbacks.credentials(
        |_url, username_from_url, _allowed_types| match private_key {
            Some(pk) => Cred::ssh_key_from_memory(username_from_url.unwrap(), None, pk, None),
            None => Cred::userpass_plaintext(
                username.expect("username missing; add username/password or private_key"),
                password.expect("password missing; add username/password or private_key"),
            ),
        },
    );

    // Prepare fetch options.
    let mut fo = git2::FetchOptions::new();
    if username.is_some() || private_key.is_some() {
        fo.remote_callbacks(callbacks);
    }

    // Prepare builder.
    let mut builder = git2::build::RepoBuilder::new();
    builder.fetch_options(fo).branch(branch);

    // Clone the project.
    let repo = builder
        .clone(uri, Path::new(path))
        .unwrap_or_else(|_| panic!("Unable to clone repository: {}", uri));
    let (object, _) = repo.revparse_ext(branch).expect("Object not found");
    let head_sha = object.id().to_string();

    let commit = object.as_commit().expect("HEAD is not a commit.");
    let atimestamp = commit.author().when().seconds();
    let atm = DateTime::<Utc>::from_utc(NaiveDateTime::from_timestamp(atimestamp, 0), Utc);
    let ctimestamp = commit.committer().when().seconds();
    let ctm = DateTime::<Utc>::from_utc(NaiveDateTime::from_timestamp(ctimestamp, 0), Utc);

    let commit = object.as_commit().expect("Unable to get object as commit.");
    let md = Metadata {
        sha: head_sha.clone(),
        author: commit.author().to_string(),
        author_date: atm,
        committer: commit.committer().to_string(),
        commit_date: ctm,
        branch: branch.to_string(),
        message: commit
            .message_raw()
            .expect("invalid message on commit")
            .to_string(),
    };

    // write the metadata to disk
    fs::write(
        Path::new(path).join(".git/.metadata"),
        serde_json::to_string(&md)?,
    )?;
    // these are kept to maintain compatibility with changes already made to
    // https://github.com/finbourne/gitlab-merge-request-resource which is a modified copy of
    // https://github.com/swisscom/gitlab-merge-request-resource
    fs::write(Path::new(path).join(".git/branch"), branch)?;
    fs::write(Path::new(path).join(".git/sha"), head_sha)?;
    fs::write(Path::new(path).join(".git/uri"), uri)?;
    Ok(md)
}
