use regex::Regex;

pub fn deconstruct_uri(uri: &str) -> (String, String, String) {
    if uri.starts_with("git@") {
        deconstruct_git_uri(uri)
    } else {
        deconstruct_url(uri)
    }
}

fn deconstruct_git_uri(uri: &str) -> (String, String, String) {
    let re = Regex::new(r"git@(.*):(.*)\.git").unwrap();
    let captures = re.captures(uri).unwrap();
    let gitlab_domain = captures.get(1).unwrap().as_str();
    let project_path = captures.get(2).unwrap().as_str();

    (
        "https".to_string(),
        gitlab_domain.to_string(),
        project_path.to_string(),
    )
}

fn deconstruct_url(uri: &str) -> (String, String, String) {
    let re = Regex::new(r"(https?)://(.+?)/(.+)").unwrap();
    let captures = re.captures(uri).unwrap();
    let protocol = captures.get(1).unwrap().as_str();
    let gitlab_domain = captures.get(2).unwrap().as_str();
    let project_path = captures.get(3).unwrap().as_str();

    (
        protocol.to_string(),
        gitlab_domain.to_string(),
        project_path.to_string(),
    )
}

#[cfg(test)]
mod tests {
    use crate::utils::uri::*;

    #[test]
    fn git_uri_decoded_correctly() {
        let uri = "git@gitlab.com:_group/_subgroup/repo.git";
        let (protocol, domain, path) = deconstruct_git_uri(uri);
        assert_eq!(protocol, "https");
        assert_eq!(domain, "gitlab.com");
        assert_eq!(path, "_group/_subgroup/repo");
    }

    #[test]
    fn https_uri_decoded_correctly() {
        let uri = "https://gitlab.com/_group/_subgroup/repo";
        let (protocol, domain, path) = deconstruct_url(uri);
        assert_eq!(protocol, "https");
        assert_eq!(domain, "gitlab.com");
        assert_eq!(path, "_group/_subgroup/repo");
    }

    #[test]
    fn http_uri_decoded_correctly() {
        let uri = "http://gitlab.com/_group/_subgroup/repo";
        let (protocol, domain, path) = deconstruct_url(uri);
        assert_eq!(protocol, "http");
        assert_eq!(domain, "gitlab.com");
        assert_eq!(path, "_group/_subgroup/repo");
    }
}
