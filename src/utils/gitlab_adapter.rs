use chrono::{DateTime, Utc};
use gitlab::api::projects::merge_requests::{
    MergeRequestOrderBy, MergeRequestState, MergeRequestsBuilder,
};
use gitlab::api::projects::repository::commits::{
    CommitBuilder, CommitStatusState, CreateCommitStatusBuilder,
};
use gitlab::api::{self, Query};
use gitlab::{Commit, Gitlab, MergeRequest};
use regex::Regex;
use serde::{Deserialize, Serialize};

pub struct MR {
    pub id: u64,
    pub iid: u64,
    pub sha: String,
    pub source_branch: String,
    pub target_branch: String,
    pub project_id: u64,
    pub work_in_progress: bool,
    pub title: String,
    pub url: String,
    pub merge_status: MergeStatus,
}

#[derive(std::cmp::PartialEq)]
pub enum MergeStatus {
    Merged,
    Other,
}

#[derive(Serialize, Deserialize, Copy, Clone, Debug)]
pub enum PipelineStatus {
    #[serde(alias = "pending")]
    Pending,
    #[serde(alias = "running")]
    Running,
    #[serde(alias = "success")]
    Success,
    #[serde(alias = "failed")]
    Failed,
    #[serde(alias = "cancelled")]
    Cancelled,
}
impl Default for PipelineStatus {
    fn default() -> Self {
        PipelineStatus::Pending
    }
}
impl From<PipelineStatus> for CommitStatusState {
    fn from(val: PipelineStatus) -> Self {
        match val {
            PipelineStatus::Pending => CommitStatusState::Pending,
            PipelineStatus::Running => CommitStatusState::Running,
            PipelineStatus::Success => CommitStatusState::Success,
            PipelineStatus::Failed => CommitStatusState::Failed,
            PipelineStatus::Cancelled => CommitStatusState::Canceled,
        }
    }
}

pub struct GitlabAdapter {
    client: Option<Gitlab>,
    get_latest_mrs_func:
        fn(client: Option<Gitlab>, project_path: &str, target_branch_regex: &str) -> Vec<MR>,
    get_last_updated_at_for_mr_func:
        fn(client: Option<Gitlab>, project_path: &str, sha: &str) -> DateTime<Utc>,
    update_pipeline_status_func: fn(
        client: Option<Gitlab>,
        project: &str,
        commit: &str,
        status: PipelineStatus,
        build_url: &str,
        label: &str,
    ),
    get_mr_details_func: fn(c: Option<Gitlab>, project_id: u64, mr_iid: u64) -> MR,
}
impl Default for GitlabAdapter {
    fn default() -> Self {
        GitlabAdapter {
            client: None,
            get_latest_mrs_func: default_get_latest_mrs,
            get_last_updated_at_for_mr_func: default_get_last_updated_at_for_mr,
            update_pipeline_status_func: default_update_pipeline_status,
            get_mr_details_func: default_get_mr_details,
        }
    }
}
impl GitlabAdapter {
    #[cfg(test)]
    pub(crate) fn new(
        latest_mrs_fn: Option<
            fn(client: Option<Gitlab>, project_path: &str, target_branch_regex: &str) -> Vec<MR>,
        >,
        last_updated_at_fn: Option<
            fn(client: Option<Gitlab>, project_path: &str, sha: &str) -> DateTime<Utc>,
        >,
        pipeline_status_fn: Option<
            fn(
                client: Option<Gitlab>,
                project: &str,
                commit: &str,
                status: PipelineStatus,
                build_url: &str,
                label: &str,
            ),
        >,
        get_mr_details_fn: Option<fn(c: Option<Gitlab>, project_id: u64, mr_iid: u64) -> MR>,
    ) -> Self {
        GitlabAdapter {
            client: None,
            get_last_updated_at_for_mr_func: match last_updated_at_fn {
                Some(f) => f,
                None => default_get_last_updated_at_for_mr,
            },
            get_latest_mrs_func: match latest_mrs_fn {
                Some(f) => f,
                None => default_get_latest_mrs,
            },
            update_pipeline_status_func: match pipeline_status_fn {
                Some(f) => f,
                None => default_update_pipeline_status,
            },
            get_mr_details_func: match get_mr_details_fn {
                Some(f) => f,
                None => default_get_mr_details,
            },
        }
    }

    pub(crate) fn set_client(&mut self, uri: &str, token: &str) {
        if self.client.is_none() && !token.is_empty() {
            let client = Gitlab::new(uri, token).unwrap();
            self.client = Some(client);
        }
    }

    pub(crate) fn get_latest_mrs(&self, path: &str, target_branch_regex: &str) -> Vec<MR> {
        let client = self.client.clone();
        (self.get_latest_mrs_func)(client, path, target_branch_regex)
    }
    pub(crate) fn get_last_updated_at_for_mr(&self, path: &str, sha: &str) -> DateTime<Utc> {
        let client = self.client.clone();
        (self.get_last_updated_at_for_mr_func)(client, path, sha)
    }

    pub(crate) fn update_pipeline_status(
        &self,
        project: &str,
        commit: &str,
        status: PipelineStatus,
        build_url: &str,
        label: &str,
    ) {
        let client = self.client.clone();
        (self.update_pipeline_status_func)(client, project, commit, status, build_url, label)
    }

    pub(crate) fn get_mr_details(&self, project_id: u64, mr_iid: u64) -> MR {
        let client = self.client.clone();
        (self.get_mr_details_func)(client, project_id, mr_iid)
    }
}

fn default_get_latest_mrs(
    c: Option<Gitlab>,
    project_path: &str,
    target_branch_regex: &str,
) -> Vec<MR> {
    let client = &c.unwrap();
    let endpoint = MergeRequestsBuilder::default()
        .project(project_path)
        .state(MergeRequestState::Opened)
        .wip(false)
        .order_by(MergeRequestOrderBy::UpdatedAt)
        .build()
        .unwrap();
    let mrs: Vec<MergeRequest> = api::paged(endpoint, api::Pagination::Limit(100))
        .query(client)
        .unwrap();

    let re = Regex::new(target_branch_regex).unwrap();
    mrs.into_iter()
        .filter(|mr| re.is_match(&mr.target_branch))
        .map(|mr| MR {
            id: mr.id.value(),
            iid: mr.iid.value(),
            sha: mr.sha.unwrap().value().to_owned(),
            source_branch: mr.source_branch,
            target_branch: mr.target_branch,
            project_id: mr.project_id.value(),
            work_in_progress: mr.work_in_progress,
            title: mr.title,
            url: mr.web_url,
            merge_status: match mr.state {
                gitlab::MergeRequestState::Merged => MergeStatus::Merged,
                _ => MergeStatus::Other,
            },
        })
        .collect()
}

fn default_get_last_updated_at_for_mr(
    c: Option<Gitlab>,
    project_path: &str,
    sha: &str,
) -> DateTime<Utc> {
    let client = &c.unwrap();
    let endpoint = CommitBuilder::default()
        .project(project_path)
        .commit(sha)
        .build()
        .unwrap();
    let commit: Commit = endpoint.query(client).unwrap();
    let cca = commit.created_at.unwrap();
    let created_at = cca.as_str();
    let date_time = DateTime::parse_from_rfc3339(created_at).unwrap();
    date_time.with_timezone(&Utc)
}

fn default_update_pipeline_status(
    c: Option<Gitlab>,
    project: &str,
    commit: &str,
    status: PipelineStatus,
    build_url: &str,
    label: &str,
) {
    let client = &c.unwrap();
    eprintln!("{build_url}");
    let endpoint = CreateCommitStatusBuilder::default()
        .project(project)
        .commit(commit)
        .state(status)
        .target_url(build_url)
        .name(label)
        .build()
        .unwrap();

    api::ignore(endpoint).query(client).unwrap_or_default();
}

fn default_get_mr_details(c: Option<Gitlab>, project_id: u64, mr_iid: u64) -> MR {
    let client = &c.unwrap();
    let endpoint = MergeRequestsBuilder::default()
        .project(project_id)
        .iid(mr_iid)
        .build()
        .unwrap();
    let mrs: Vec<MergeRequest> = api::paged(endpoint, api::Pagination::Limit(100))
        .query(client)
        .unwrap();
    // there can be only one.
    let mr = mrs[0].clone();
    MR {
        id: mr.id.value(),
        iid: mr.iid.value(),
        sha: mr.sha.unwrap().value().to_owned(),
        source_branch: mr.source_branch,
        target_branch: mr.target_branch,
        project_id: mr.project_id.value(),
        work_in_progress: mr.work_in_progress,
        title: mr.title,
        url: mr.web_url,
        merge_status: match mr.state {
            gitlab::MergeRequestState::Merged => MergeStatus::Merged,
            _ => MergeStatus::Other,
        },
    }
}
